import React from 'react';
import './App.css';
import Amplify, { Auth } from 'aws-amplify';
import awsconfig from './aws-exports';
import { withAuthenticator } from 'aws-amplify-react';
Amplify.configure(awsconfig);

/*
Auth.currentAuthenticatedUser()
.then(user => console.log(user))
.catch(err => console.log(err));
*/

function App() {
  return (
    <div className="App">
      <header className="App-header">
      </header>
    </div>
  );
}

export default withAuthenticator(App, { includeGreetings: true });
